# Overview of Cluster Setup

- Create postgresql.conf for all PostgreSQL nodes.
- Start master.
- Create replication user.
- Create standby node.
- Create pg_hba.conf for participating nodes and distribute to all nodes.
- Configure standby node.
- Clear PGDATA directory on standby node.
- Use `pg_basebackup` to sync master configuration to standby node.
- Place .pgpass file on standby node.
- Start standby node.

# Promoting the Standby Node

- Since we haven't set trigger_file in recovery.conf on the standby node, run `PGDATA=/var/lib/postgresql/data pg_ctl promote` on the standby node you wish to make primary.
- If `trigger_file` is set to a filesystem path and that file is created, the standby will end recovery mode.
