# PostgreSQL Replication Discovery

This project provides a Docker Compose setup containing a few Postgres nodes to experiement with replication options.

Generally, you'll execute the following steps to produce a primary-hotstandby configuration:

- `make up` Spin up the database nodes.
- `make primary` Creates the primary node.
- `make standby` Creates the standby node.

Other tasks:

- `make clean` Nuke the cluster.
- `make fail_primary` Stop the primary and make the standby the master node.

You can flipflop the master/standby by setting `PRIMARY` and `SECONDARY` e.g.:

- `ruby app/main.rb`
- `make up`
- `make primary`
- `make standby`
- `make fail_primary`
- `make reload_pgbouncer`
- `PRIMARY=db2 STANDBY=db1 make standby`

At this point, `db1`, which started out as primary is now a secondary and `db2`, which started out as a standby is now a primary.

# Connection Pooling

This setup includes PgBouncer and you may connect to it on the host bound port, `5432`.

# Example Application

`app/main.rb` is a test application that connects to pgbouncer to test failure scenarios. You can run it, `ruby app/main.rb`, fail the primary, `make fail_primary` and it should not be disconnected.
