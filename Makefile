 PRIMARY ?= db1
 STANDBY ?= db2

clean:
	docker-compose down
	docker-compose rm -f
	sudo rm -rf data/db* .pgpass

up: pgpass
	docker-compose up -d

pgpass:
	./scripts/pgpass.sh

primary:
	./scripts/primary.sh ${PRIMARY} ${STANDBY}

standby:
	./scripts/standby.sh ${PRIMARY} ${STANDBY}

fail_primary:
	docker-compose stop ${PRIMARY}
	docker-compose exec -u postgres -e PGDATA=/var/lib/postgresql/data ${STANDBY} pg_ctl promote
	sed -i 's/${PRIMARY}/${STANDBY}/g' ${PWD}/etc/pgbouncer/pgbouncer.ini

reload_pgbouncer:
	docker-compose exec pgbouncer kill -SIGHUP 1

wal_status:
	docker-compose exec db1 ps -ef | grep "wal" || true
	docker-compose exec db2 ps -ef | grep "wal" || true

recovery_status:
	docker-compose exec db1 psql -U db -c "select pg_is_in_recovery();" || true 
	docker-compose exec db2 psql -U db -c "select pg_is_in_recovery();" || true 
