require "bundler/inline"

gemfile do
  source "https://rubygems.org"
  gem "faker"
  gem "pg"
end

require "faker"
require "pg"

# Number of threads to spin up
thread_count = 10

def log(message)
  puts "%s: %s\n----" % [Time.now, message]
end

def get_connection()
  loop do
    begin
      conn = PG.connect(host: "localhost", dbname: "db", user: "db", password: "db")
      prepared_statement = conn.prepare("insert_chuck_norris", "INSERT INTO gear_names (name) VALUES ($1)")
      return conn
    rescue StandardError => e
      log(e)
      sleep 2
    end
  end
end

def db_stuffs(conn)
  loop do
    begin
      conn.exec_prepared("insert_chuck_norris", [Faker::ChuckNorris.fact])
      conn.exec("SELECT * FROM gear_names") do |result|
        log("gear_name count: %s" % result.ntuples)
      end
    rescue PG::ConnectionBad, PG::UnableToSend, PG::ReadOnlySqlTransaction => e
      log(e)
      conn = get_connection()
    end

    #sleep 1
  end
end

threads = []
thread_count.times {
  threads << Thread.new do
    conn = get_connection()
    db_stuffs(conn)
  end
}
threads.each(&:join)
