#!/bin/bash

set -u

source .env
source ./scripts/common.sh

primary=$1
standby=$2

# Configure standby node.
configure_standby "$standby" "$primary"
