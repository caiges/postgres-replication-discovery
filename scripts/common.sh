configure_replication() {
  local primary=$1
  local standby=$2

  # Get pgbouncer ip.
  pgbouncer_ip=`docker inspect pgbouncer -f '{{ .NetworkSettings.Networks.db.IPAddress }}'`

  # Get primary ip.
  primary_ip=`docker inspect $primary -f '{{ .NetworkSettings.Networks.db.IPAddress }}'`

  # Get standby ip.
  standby_ip=`docker inspect $standby -f '{{ .NetworkSettings.Networks.db.IPAddress }}'`

  # Create replication user.
  docker-compose exec $primary psql -h $primary -p 5432 -U $POSTGRES_USER -c "CREATE ROLE $REPLICATION_USER WITH REPLICATION PASSWORD '$REPLICATION_PASSWORD' LOGIN;"

  # Allow connection from pgbouncer.
  docker-compose exec $primary bash -c "echo \"host $POSTGRES_USER $POSTGRES_USER $pgbouncer_ip/32 md5\" >> $PGDATA/pg_hba.conf"

  # Allow connection from primary (itself in the case that primary switches to standby).
  docker-compose exec $primary bash -c "echo \"host replication $REPLICATION_USER $primary_ip/32 md5\" >> $PGDATA/pg_hba.conf"

  # Allow connection from standby.
  docker-compose exec $primary bash -c "echo \"host replication $REPLICATION_USER $standby_ip/32 md5\" >> $PGDATA/pg_hba.conf"

  docker-compose restart $primary
}

configure_standby() {
  local standby=$1
  local primary=$2
  
  docker-compose stop $standby

  # Clean out standby data directory.
  docker-compose run --rm $standby bash -c "rm -rf $PGDATA/*"

  # Sync data from primary.
  docker-compose run --rm $standby pg_basebackup -h $primary -p 5432 -R -D $PGDATA -P -U $REPLICATION_USER -X stream

  # Chown pgpassfile to postgres user so Postgres will use it (will refuse otherwise).
  docker-compose run --rm $standby chown postgres:postgres $PGPASSFILE
  
  docker-compose start $standby
}
