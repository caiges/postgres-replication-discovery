#!/bin/bash

source .env
source ./scripts/common.sh

# Generate pgpass file.
cat << EOF > .pgpass
db1:5432:$POSTGRES_USER:$POSTGRES_USER:$POSTGRES_PASSWORD
db1:5432:replication:$REPLICATION_USER:$REPLICATION_PASSWORD
db2:5432:$POSTGRES_USER:$POSTGRES_USER:$POSTGRES_PASSWORD
db2:5432:replication:$REPLICATION_USER:$REPLICATION_PASSWORD
EOF
chmod 600 .pgpass
sudo chown root:root .pgpass
